from os import environ as env

from dotenv import find_dotenv, load_dotenv
from flask import Flask, render_template

from api.bp_access import bp_access
from api.bp_api import bp_api
from api.bp_auth import bp_auth
from api.bp_cache import bp_cache
from api.bp_click import bp_click
from api.bp_cmd import bp_cmd
from api.bp_cors import bp_cors
from api.bp_csrf import bp_csrf
from api.bp_deserialization import bp_deserialization
from api.bp_dom import bp_dom
from api.bp_essental import bp_essential
from api.bp_graphql import bp_graphql
from api.bp_host import bp_host
from api.bp_info import bp_info
from api.bp_jwt import bp_jwt
from api.bp_llm import bp_llm
from api.bp_logic import bp_logic
from api.bp_nosql import bp_nosql
from api.bp_oauth import bp_oauth
from api.bp_path import bp_path
from api.bp_prototype import bp_prototype
from api.bp_race import bp_race
from api.bp_request import bp_request
from api.bp_sql import bp_sql
from api.bp_ssrf import bp_ssrf
from api.bp_ssti import bp_ssti
from api.bp_upload import bp_upload
from api.bp_websocket import bp_websocket
from api.bp_xss import bp_xss
from api.bp_xxe import bp_xxe


# .env
ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)

# Flask
app = Flask(__name__)
app.config['SECRET_KEY'] = env.get('APP_SECRET_KEY')

for bp in [bp_access, bp_api, bp_auth, bp_cache, bp_click, bp_cmd, bp_cors, bp_csrf, bp_deserialization, bp_dom,
           bp_essential, bp_graphql, bp_host, bp_info, bp_jwt, bp_llm, bp_logic, bp_nosql, bp_oauth, bp_path,
           bp_prototype, bp_race, bp_request, bp_sql, bp_ssrf, bp_ssti, bp_upload, bp_websocket, bp_xss, bp_xxe]:
    app.register_blueprint(bp, url_prefix='/api')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/swagger')
def swagger():
    return app.send_static_file('json/swagger.json')


if __name__ == '__main__':
    print(app.url_map)
    app.run(host='0.0.0.0', port=env.get('PORT', 5000), debug=True)
